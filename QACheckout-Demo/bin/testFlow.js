/**
 * Created by manthanb on 11/11/2016.
 */
require('mocha');
var fs = require('fs');


var variables = require('./variables60.js');


var userAuth = require('./userAuthentication.js');
var session = require('./sessionInitialize.js');
var interfaceOperations = require('./interfaces.js');
var notificationOperations = require('./notification.js');
var accessControlOperations = require('./accessControl.js');
var hrmEntityOperations = require('./hrmEntity.js');
var orgOperations = require('./organization.js');
var caseOperations = require('./case.js');
var taskOperations = require('./task.js');
var memoOperations = require('./memo.js');
var campaignOperations = require('./campaign.js');
var contactOperations = require('./contact.js');
var opportunityOperations = require('./opportunity.js');
var searchOperations = require('./search.js');
var patientOperations = require('./patient.js');
var calendarOperations = require('./calendar.js');
var providerOperations = require('./provider.js');
var messageOperations = require('./message.js');
var templateOperations = require('./workflowtemplate.js');

describe("happy path test for " + variables.fullUrl, function () {

    // var length = variables.casesToExecute[];
   /* for(var i in variables.casesToExecute) {
        console.log (i, variables.casesToExecute[i])
        var fn =   i+'.'+variables.casesToExecute[i].trim()+'()';

        eval(fn);
    }*/

    var length = variables.casesToExecute.length;
    for (var execute=0;execute<length;execute++){
        var fnName = variables.casesToExecute[execute].trim()+'()';
        // fnName += '()';
        console.log(fnName);
        eval(fnName);

    }


    /*console.log(length);
    console.log(variables.casesToExecute[0]);
    console.log(variables.casesToExecute[1]);
    console.log(variables.casesToExecute[2]);*/

});
