/**
 * Created by INDUSA on 9/22/2016.
 */

require('mocha');
var _ = require('lodash');
var request = require('request').defaults({
    jar: true
});
var rp = require('request-promise').defaults({
    jar: true
});
var bluebird = require('bluebird');

var variables = require('./variables60.js');
var chai = require('chai');
var should = chai.should();
var expect = require('chai').expect;
var fs = require('fs');
var isLoggedOut = false;
var me = require('./genericOperations.js');
var logging = require('log4js');
var testLog = require('log4js');
var logfilename = 'genericOperations';

/*testLog.configure({
    appenders: [{
    "type": "dateFile",
    "pattern": "-yyyy-MM-dd",
    // "filename": "logs/test.log",
    "alwaysIncludePattern": true
}],replaceConsole: false

});*/

/*logging.configure({
    appenders: [
        {
            type: 'console',
            layout: {
                type: 'pattern',
                pattern: "[%r] [%[%5.5p%]] - %m%n"
            }
        }
         ,
        {
            type: "file",
            filename: variables.logFilePath+"console.log",
            category: [ 'console','console']
        }
    ],
    replaceConsole: false
});*/

exports.callRequest = function callRequest(defPath, requestJson, call) {
    var path = defPath;
    var body = requestJson;
    var url = variables.fullUrl + path;
    var options = {
        method: 'POST',
        url: url,
        rejectUnauthorized: false,
        headers: {
            'Content-Type': 'application/json'
        },
        body: body
    };
    //console.log(options); //@NOTE: For debugging to see the request JSON
    return rp(options).then(function (body) {
        var response = JSON.parse(body);
        if (call == "login" && response.expired === false) {
            console.log("Successful " + call);
        } else {
            console.log("Call: " + call);
        };
        //console.log(response); //@NOTE: For debugging to see the response JSON
        return bluebird.resolve(response);
        /*@NOTE JC: This seems redundant. I believe this is already inside of a promise, so no need to create and return another promise
         Response: I need this to get the response back out of the function*/
    }).catch(function (err) {

        //Added By INDUSA 11/03/2016 : For Using in User Failed Logged Out Test Case - START
        if (call === "verifyLogout") {
            console.log("User already logged out");
            isLoggedOut = true;
            throw new Error(isLoggedOut);
        }
        //Added By INDUSA 11/03/2016 : For Using in User Failed Logged Out Test Case - END
        console.log("Post Failed " + err);
        if (call === "login") {
            console.log("Login failed, shutting down test.");
            //throw new Error("fail"); //@NOTE: this just failed the test but did not stop the test.
            process.exit(1); //@NOTE: exit the call if login doesn't work
        }
    });
}

//This Function returns value for specific element to get validated
var checkData = function (dataOfElement, hc1Object, element) {
    //Added By INDUSA 09/19/2016 - START

    //Condition to see if dataOfElement is Object or not.
    if (typeof dataOfElement !== "object") { //@FIXME: Manthan, I can't figure this out. I am trying to see if this is an object if not get the value, if so, get the value of .name and show that.
        if (dataOfElement !== null) {
            //console.log("=====> " + hc1Object + " object's element " + element + " is found with value : " + dataOfElement);
            return dataOfElement;
        } else {
            return ("=====> " + element + " is NOT Correct OR NULL in  " + hc1Object + " object ");
        }
    } else {
        var jsonData = JSON.stringify(dataOfElement);
        var parsedJSON = JSON.parse(jsonData);
        var elementValue = '';

        //For Determining IF JSON Data is Null Or Undefined
        if (parsedJSON === null || parsedJSON === undefined) {
            console.log('=====> Null OR undefined JSON Object Found ');
        } else {
            var arrayConstructor = [].constructor;
            var objectConstructor = {}.constructor;
            var dataConstructor = parsedJSON.constructor;

            //For Determining JSON Array
            if (dataConstructor === arrayConstructor) {

                for (var i = 0; i < parsedJSON.length; i++) {
                    console.log("=====> JSONArray Found : " + element);
                    elementValue = parsedJSON[i].name;
                }
            // For Determining JSON Object
            } else if (dataConstructor === objectConstructor) {
                console.log("=====> JSONObject Found : " + element);
                elementValue = parsedJSON.name;
            }
        }
        return elementValue;
    };
    //Added By INDUSA 09/19/2016 - END
};
exports.checkData = checkData;


//This Function validates Case Object
var validateCaseData = function (dataOfElement,hc1Object,retryCount,logfilename) {

    var loggedInUser = variables.userFirstName + " "+variables.userLastName;
    console.log('=====> Validating CASE  : validateCaseData ');

    me.validateData("caseActive",checkData(dataOfElement.active, hc1Object, "active"),variables.caseActive,retryCount,logfilename);
    me.validateData("caseActivityCategory",checkData(dataOfElement.category, hc1Object, "category"),variables.caseActivityCategory[1],retryCount,logfilename);
    me.validateData("casesubject",checkData(dataOfElement.subject, hc1Object, "subject"),variables.caseSubject,retryCount,logfilename);
    me.validateData("caseActivitySubCategory",checkData(dataOfElement.subCategory, hc1Object, "subCategory"),variables.caseActivitySubCategory[1],retryCount,logfilename);
    me.validateData("organizationName",checkData(dataOfElement.organization, hc1Object, "organization.name"),variables.orgName,retryCount,logfilename);
    me.validateData("caseDescription",checkData(dataOfElement.description, hc1Object, "description"),variables.caseDescription,retryCount,logfilename);
    me.validateData("caseCaseNumber",checkData(dataOfElement.number, hc1Object, "number"),variables.caseCaseNumber,retryCount,logfilename);
    me.validateData("createdUser",checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("updatedUser",checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("casePriorityName",checkData(dataOfElement.priority, hc1Object, "priority.name"),variables.casePriorityName,retryCount,logfilename);
    me.validateData("assignedTo",checkData(dataOfElement.assignedTo, hc1Object, "assignedTo.name"),variables.userLastName+", "+variables.userFirstName,retryCount,logfilename);
    me.validateData("caseActivityStatus",checkData(dataOfElement.status, hc1Object, "status.name"),variables.caseActivityStatus,retryCount,logfilename);
    me.validateData("caseResolvedBy",checkData(dataOfElement.resolvedBy, hc1Object, "resolvedBy.name"),variables.caseResolvedBy,retryCount,logfilename);
    me.validateData("caseCorrectiveAction",checkData(dataOfElement.correctiveAction, hc1Object, "correctiveAction"),variables.caseCorrectiveAction,retryCount,logfilename);
    me.validateData("caseResolution",checkData(dataOfElement.resolutionDescription, hc1Object, "resolutionDescription"),variables.caseResolution,retryCount,logfilename);
    me.validateData("caseRootCause",checkData(dataOfElement.rootCause, hc1Object, "rootCause"),variables.caseRootCause,retryCount,logfilename);
    me.validateData("caseCreatedDate",checkData(dataOfElement.createdDate, hc1Object, "createdDate"),variables.caseCreatedDate,retryCount,logfilename);
    me.validateData("caseDate",checkData(dataOfElement.date, hc1Object, "date"),variables.caseDate,retryCount,logfilename);
    me.validateData("caseUpdatedDate",checkData(dataOfElement.updatedDate, hc1Object, "updatedDate"),variables.caseUpdatedDate,retryCount,logfilename);
    me.validateData("caseResolvedOn",checkData(dataOfElement.resolvedDate, hc1Object, "resolvedDate"),variables.caseResolvedOn,retryCount,logfilename);
};
exports.validateCaseData = validateCaseData;

//This Function validates Task Object
var validateTaskData = function (dataOfElement,hc1Object,retryCount,logfilename) {

    var loggedInUser = variables.userFirstName + " "+variables.userLastName;
    console.log('=====> Validating TASK  : validateTaskData ');

    me.validateData("taskActive",checkData(dataOfElement.active, hc1Object, "active"),variables.taskActive,retryCount,logfilename);
    me.validateData("taskSubjecty",checkData(dataOfElement.subject, hc1Object, "subject"),variables.taskSubject,retryCount,logfilename);
    me.validateData("taskActivityCategory",checkData(dataOfElement.category, hc1Object, "category"),variables.taskActivityCategory[0],retryCount,logfilename);
    me.validateData("taskActivitySubCategory",checkData(dataOfElement.subCategory, hc1Object, "subCategory"),variables.taskActivitySubCategory[0],retryCount,logfilename);
    me.validateData("taskDescription",checkData(dataOfElement.description, hc1Object, "description"),variables.taskDescription,retryCount,logfilename);
    me.validateData("organizationName",checkData(dataOfElement.organization, hc1Object, "organization.name"),variables.orgName,retryCount,logfilename);
    me.validateData("createdUser",checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("updatedUser",checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("taskCaseNumber",checkData(dataOfElement.number, hc1Object, "number"),variables.taskCaseNumber,retryCount,logfilename);
    me.validateData("taskPriorityName",checkData(dataOfElement.priority, hc1Object, "priority.name"),variables.taskPriorityName,retryCount,logfilename);
    me.validateData("taskAssignedToUser",checkData(dataOfElement.assignedTo, hc1Object, "assignedTo.name"),variables.taskAssignedToUser,retryCount,logfilename);
    me.validateData("taskActivityStatus",checkData(dataOfElement.status, hc1Object, "status.name"),variables.taskActivityStatus,retryCount,logfilename);
    me.validateData("taskDate",checkData(dataOfElement.date, hc1Object, "date"),variables.taskDate,retryCount,logfilename);
    me.validateData("taskBeginDate",checkData(dataOfElement.beginDate, hc1Object, "beginDate"),variables.taskBeginDate,retryCount,logfilename);
    me.validateData("taskCreatedDate",checkData(dataOfElement.createdDate, hc1Object, "createdDate"),variables.taskCreatedDate,retryCount,logfilename);
    me.validateData("taskUpdatedDate",checkData(dataOfElement.updatedDate, hc1Object, "updatedDate"),variables.taskUpdatedDate,retryCount,logfilename);
    me.validateData("taskDueDate",checkData(dataOfElement.dueDate, hc1Object, "dueDate"),variables.taskDueDate,retryCount,logfilename);

};
exports.validateTaskData = validateTaskData;

//This Function validates Memo Object
var validateMemoData = function (dataOfElement,hc1Object,retryCount,logfilename) {

    var loggedInUser = variables.userFirstName + " "+variables.userLastName;
    console.log('=====> Validating MEMO  : validateMemoData ');

    me.validateData("memoActive",checkData(dataOfElement.active, hc1Object, "active"),variables.memoActive,retryCount,logfilename);
    me.validateData("memoSubject",checkData(dataOfElement.subject, hc1Object, "subject"),variables.memoSubject,retryCount,logfilename);
    me.validateData("memoActivityCategory",checkData(dataOfElement.category, hc1Object, "category"),variables.memoActivityCategory[0],retryCount,logfilename);
    me.validateData("memoActivitySubCategory",checkData(dataOfElement.subCategory, hc1Object, "subCategory"),variables.memoActivitySubCategory[0],retryCount,logfilename);
    me.validateData("organizationName",checkData(dataOfElement.organization, hc1Object, "organization.name"),variables.orgName,retryCount,logfilename);
    me.validateData("memoDescription",checkData(dataOfElement.description, hc1Object, "description"),variables.memoDescription,retryCount,logfilename);
    me.validateData("createdUserName",checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("updatedUserName",checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("memoCaseNumber",checkData(dataOfElement.number, hc1Object, "number"),variables.memoCaseNumber,retryCount,logfilename);
    me.validateData("memoDate",checkData(dataOfElement.date, hc1Object, "date"),variables.memoDate,retryCount,logfilename);
    me.validateData("memoBeginDate",checkData(dataOfElement.beginDate, hc1Object, "beginDate"),variables.memoBeginDate,retryCount,logfilename);
    me.validateData("memoCreatedDate",checkData(dataOfElement.createdDate, hc1Object, "createdDate"),variables.memoCreatedDate,retryCount,logfilename);
    me.validateData("memoUpdatedDate",checkData(dataOfElement.updatedDate, hc1Object, "updatedDate"),variables.memoUpdatedDate,retryCount,logfilename);


    //should.equal(actual,expected,message)
    //should.equal(checkData(dataOfElement.active, hc1Object, "active"),variables.memoActive, "memoActive value didn't match");
    //should.equal(checkData(dataOfElement.subject, hc1Object, "subject"),variables.memoSubject, "subject value didn't match");
    //should.equal(checkData(dataOfElement.category, hc1Object, "category"),variables.memoActivityCategory[0], "category value didn't match");
    //should.equal(checkData(dataOfElement.subCategory, hc1Object, "subCategory"),variables.memoActivitySubCategory[0], "subCategory value didn't match");
    //should.equal(checkData(dataOfElement.organization, hc1Object, "organization.name"),variables.orgName, "organization value didn't match");
    //should.equal(checkData(dataOfElement.description, hc1Object, "description"),variables.memoDescription, "description value didn't match");
    //should.equal(checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),loggedInUser, "createdUser value didn't match");
    //should.equal(checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),loggedInUser, "updatedUser value didn't match");
    //should.equal(checkData(dataOfElement.number, hc1Object, "number"),variables.memoCaseNumber, "number value didn't match");
    //should.equal(checkData(dataOfElement.date, hc1Object, "date"),variables.memoDate, "date value didn't match");
    //should.equal(checkData(dataOfElement.beginDate, hc1Object, "beginDate"),variables.memoBeginDate, "beginDate value didn't match");
    //should.equal(checkData(dataOfElement.createdDate, hc1Object, "createdDate"),variables.memoCreatedDate, "createdDate value didn't match");
    //should.equal(checkData(dataOfElement.updatedDate, hc1Object, "updatedDate"),variables.memoUpdatedDate, "updatedDate value didn't match");

};
exports.validateMemoData = validateMemoData;


//This Function validates Patient Object
var validatePatientData = function (dataOfElement,hc1Object,caseName,logfilename,retryCount) {

    var loggedInUser = variables.userFirstName + " "+variables.userLastName;
    var salutationName = variables.patientSalutation+'.';
    console.log('=====> Validating Patient  : validatePatientData ');

    me.validateData("patientActive",checkData(dataOfElement.active, hc1Object, "active"),variables.patientActive,retryCount,logfilename);
    me.validateData("patientFirstName",checkData(dataOfElement.firstName, hc1Object, "firstName"),variables.patientFirstName,retryCount,logfilename);
    me.validateData("patientmiddleName",checkData(dataOfElement.middleName, hc1Object, "middleName"),variables.patientMiddleName,retryCount,logfilename);
    me.validateData("patientLastName",checkData(dataOfElement.lastName, hc1Object, "lastName"),variables.patientLastName,retryCount,logfilename);
    me.validateData("patientEmail",checkData(dataOfElement.email, hc1Object, "email"),variables.patientEmail,retryCount,logfilename);
    me.validateData("patientSalutation",checkData(dataOfElement.salutation, hc1Object, "salutation.name"),salutationName,retryCount,logfilename);
    me.validateData("availableOrganizations",checkData(dataOfElement.availableOrganizations, hc1Object, "availableOrganizations.name"),variables.orgName,retryCount,logfilename);
    me.validateData("createdUser",checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("updatedUser",checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),loggedInUser,retryCount,logfilename);
};
exports.validatePatientData = validatePatientData;


//This Function validates Contact Object
var validateContactData = function (dataOfElement,hc1Object,retryCount,logfilename) {

    var loggedInUser = variables.userFirstName + " "+variables.userLastName;
    console.log('=====> Validating Contact  : validateContactData ');

    me.validateData("contactActive",checkData(dataOfElement.active, hc1Object, "active"),variables.contactActive,retryCount,logfilename);
    me.validateData("contactFirstName",checkData(dataOfElement.firstName, hc1Object, "firstName"),variables.contactFirstName,retryCount,logfilename);
    me.validateData("contactMiddleName",checkData(dataOfElement.middleName, hc1Object, "middleName"),variables.contactMiddleName,retryCount,logfilename);
    me.validateData("contactLastName",checkData(dataOfElement.lastName, hc1Object, "lastName"),variables.contactLastName,retryCount,logfilename);
    me.validateData("contactEmail",checkData(dataOfElement.email, hc1Object, "email"),variables.contactEmail,retryCount,logfilename);
    me.validateData("contactCreatedDate",checkData(dataOfElement.createdDate, hc1Object, "createdDate"),variables.contactCreatedDate,retryCount,logfilename);
    me.validateData("createdUserName",checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),loggedInUser,retryCount,logfilename);
    me.validateData("updatedDate",checkData(dataOfElement.updatedDate, hc1Object, "updatedDate"),variables.contactUpdatedDate,retryCount,logfilename);
    me.validateData("updatedUserName",checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),loggedInUser,retryCount,logfilename);



    //should.equal(actual,expected,message)
    //should.equal(checkData(dataOfElement.active, hc1Object, "active"),variables.contactActive, "patientActive value didn't match");
    //should.equal(checkData(dataOfElement.firstName, hc1Object, "firstName"),variables.contactFirstName, "firstName value didn't match");
    //should.equal(checkData(dataOfElement.middleName, hc1Object, "middleName"),variables.contactMiddleName, "middleName value didn't match");
    //should.equal(checkData(dataOfElement.lastName, hc1Object, "lastName"),variables.contactLastName, "lastName value didn't match");
    //should.equal(checkData(dataOfElement.email, hc1Object, "email"),variables.contactEmail, "email value didn't match");
    //should.equal(checkData(dataOfElement.createdDate, hc1Object, "createdDate"),variables.contactCreatedDate, "createdDate value didn't match");
    //should.equal(checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),loggedInUser, "createdUser value didn't match");
    //should.equal(checkData(dataOfElement.updatedDate, hc1Object, "updatedDate"),variables.contactUpdatedDate, "updatedDate value didn't match");
    //should.equal(checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),loggedInUser, "updatedUser value didn't match");
    // should.equal(checkData(dataOfElement.availableOrganizations, hc1Object, "availableOrganizations.name"),variables.orgName, "organization value didn't match");

};
exports.validateContactData = validateContactData;


//This Function validates Opportunity Object
var validateOpportunityData = function (dataOfElement,hc1Object,retryCount,logfilename) {

    var currentUser = variables.userLastName+", "+variables.userFirstName;
    console.log('=====> Validating Opportunity  : validateOpportunityData ');

    me.validateData("opportunityActive",checkData(dataOfElement.active, hc1Object, "active"),variables.opportunityActive,retryCount,logfilename);
    me.validateData("opportunityDescription",checkData(dataOfElement.description, hc1Object, "description"),variables.opportunityDescription,retryCount,logfilename);
    me.validateData("organizationName",checkData(dataOfElement.organization, hc1Object, "organization.name"),variables.extractOrgName,retryCount,logfilename);
    me.validateData("salesRepName",checkData(dataOfElement.salesRep, hc1Object, "salesRep.name"),currentUser,retryCount,logfilename);
    me.validateData("salesTerritoryName",checkData(dataOfElement.salesTerritory, hc1Object, "salesTerritory.name"),variables.salesTerritoryName,retryCount,logfilename);
    me.validateData("opportunityNumber",checkData(dataOfElement.opportunityNumber, hc1Object, "opportunityNumber"),variables.extractOpportunityNumber,retryCount,logfilename);
    me.validateData("effectiveDate",checkData(dataOfElement.effectiveDate, hc1Object, "effectiveDate"),variables.opportunityEffectiveDate,retryCount,logfilename);
    me.validateData("opportunityCloseDate",checkData(dataOfElement.closeDate, hc1Object, "opportunityCloseDate"),variables.opportunityCloseDate,retryCount,logfilename);
    me.validateData("createdUserName",checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),currentUser,retryCount,logfilename);
    me.validateData("updatedUserName",checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),currentUser,retryCount,logfilename);

    /*//should.equal(actual,expected,message)
    should.equal(checkData(dataOfElement.active, hc1Object, "active"),variables.opportunityActive, "opportunityActive value didn't match");
    should.equal(checkData(dataOfElement.description, hc1Object, "description"),variables.opportunityDescription, "opportunityDescription value didn't match");
    should.equal(checkData(dataOfElement.organization, hc1Object, "organization.name"),variables.extractOrgName, "organization value didn't match");
    should.equal(checkData(dataOfElement.salesRep, hc1Object, "salesRep.name"),currentUser, "salesRep value didn't match");
    should.equal(checkData(dataOfElement.salesTerritory, hc1Object, "salesTerritory.name"),variables.salesTerritoryName, "salesTerritory value didn't match");
    should.equal(checkData(dataOfElement.opportunityNumber, hc1Object, "opportunityNumber"),variables.extractOpportunityNumber, "opportunityNumber value didn't match");
    should.equal(checkData(dataOfElement.effectiveDate, hc1Object, "effectiveDate"),variables.opportunityEffectiveDate, "effectiveDate value didn't match");
    should.equal(checkData(dataOfElement.closeDate, hc1Object, "opportunityCloseDate"),variables.opportunityCloseDate, "opportunityCloseDate value didn't match");
    should.equal(checkData(dataOfElement.createdUser, hc1Object, "createdUser.name"),currentUser, "createdUser value didn't match");
    should.equal(checkData(dataOfElement.updatedUser, hc1Object, "updatedUser.name"),currentUser, "updatedUser value didn't match");*/

};
exports.validateOpportunityData = validateOpportunityData;


//This function validates new provider contact
var validateProviderData = function (dataOfElement,hc1Object,retryCount,logfilename){
    var currentUser = variables.userFirstName+" "+variables.userLastName;
    console.log('=====> Validating Contact  : validateProviderData ');
    me.validateData("providerId",checkData(dataOfElement.id, hc1Object, "contact.id"),variables.providerId,retryCount,logfilename);
    me.validateData("providerName",checkData(dataOfElement.name, hc1Object, "contact.name"),variables.providerName,retryCount,logfilename);
    me.validateData("createdUserName",checkData(dataOfElement.createdUser.name, hc1Object, "contact.createdUser.name"),currentUser,retryCount,logfilename);
    me.validateData("updatedUserName",checkData(dataOfElement.updatedUser.name, hc1Object, "contact.updatedUser.name"),currentUser,retryCount,logfilename);
    me.validateData("createdUserEmail",checkData(dataOfElement.createdUser.email, hc1Object, "contact.createdUser.email"),variables.userEmail,retryCount,logfilename);
    me.validateData("availableOrganizationName",checkData(dataOfElement.availableOrganizations, hc1Object, "availableOrganizations.name"),variables.extractOrgName,retryCount,logfilename);

/*
    should.equal(checkData(dataOfElement.id, hc1Object, "contact.id"),variables.providerId, "contact Id value didn't match");
    should.equal(checkData(dataOfElement.name, hc1Object, "contact.name"),variables.providerName, "contact name value didn't match");
    should.equal(checkData(dataOfElement.createdUser.name, hc1Object, "contact.createdUser.name"),currentUser, "Created user name value didn't match");
    should.equal(checkData(dataOfElement.updatedUser.name, hc1Object, "contact.updatedUser.name"),currentUser, "Updated user name value didn't match");
    should.equal(checkData(dataOfElement.createdUser.email, hc1Object, "contact.createdUser.email"),variables.userEmail, "User email value didn't match");
    should.equal(checkData(dataOfElement.availableOrganizations, hc1Object, "availableOrganizations.name"),variables.extractOrgName, "Organization name value didn't match");
*/
};
exports.validateProviderData = validateProviderData;


//This function validates new Patient info
var validateCampaignData = function (dataOfElement,hc1Object,retryCount,logfilename){
    var currentUser = variables.userFirstName+" "+variables.userLastName;
    console.log('=====> Validating Campaign Info  : validateCampaignData');

    me.validateData("campaignAddName",checkData(dataOfElement.name, hc1Object, "campaign.name"),variables.campaignAddName,retryCount,logfilename);
    me.validateData("campaignActive",checkData(dataOfElement.active, hc1Object, "campaign.active"),true,retryCount,logfilename);
    me.validateData("campaignTargetType",checkData(dataOfElement.targetType, hc1Object, "campaign.targetType"),variables.campaignTargetType,retryCount,logfilename);
    me.validateData("campaignNumber",checkData(dataOfElement.campaignNo, hc1Object, "campaign.campaignNo"),variables.campaignNo,retryCount,logfilename);
    me.validateData("campaignTypeName",checkData(dataOfElement.campaignType.name, hc1Object, "campaign.campaignType.name"),variables.campaignTypeNames[0],retryCount,logfilename);
    me.validateData("createdUserName",checkData(dataOfElement.createdUser.name, hc1Object, "campaign.createdUser.name"),variables.username,retryCount,logfilename);
    me.validateData("createdUserEmail",checkData(dataOfElement.createdUser.email, hc1Object, "campaign.createdUser.email"),variables.userEmail,retryCount,logfilename);
    me.validateData("updatedUserName",checkData(dataOfElement.updatedUser.name, hc1Object, "campaign.updatedUser.name"),variables.username,retryCount,logfilename);
    me.validateData("updatedUserEmail",checkData(dataOfElement.updatedUser.email, hc1Object, "campaign.updatedUser.email"),variables.userEmail,retryCount,logfilename);

};
exports.validateCampaignData = validateCampaignData;


//This Function validates Objects stored inside Calendar
var validateCalendarData = function (event,retryCount,logfilename) {
    console.log('=====> Validating Calendar Data  : validateCalendarData ');
    var recExtractId = event.detailUrl.split("/");
    var recExtractEventDetails = event.name.split(" | ");
    var objectType = event.primaryRelatedItemType;
    console.log('objectType : ' + objectType);
    var recExtractObjectId = recExtractId[recExtractId.length - 1];
    var recExtractObjectSubject = recExtractEventDetails[0];
    var recExtractObjectOrg = recExtractEventDetails[recExtractEventDetails.length - 1]
    console.log('------------------------------------------------------------------------------')
    //TODO : @Eric, Currently we are just searching Task Object from calendar response let us know if any other objects need to be added
    if (objectType === 'Task') {

        console.log('=====> Task Object Found In Calendar With Below Details');
        console.log('=====> Task Id : ' + recExtractObjectId);
        console.log('=====> Task Subject : ' + recExtractObjectSubject);
        console.log('=====> Org Name : ' + recExtractObjectOrg);

        if (recExtractObjectSubject != undefined && recExtractObjectId != undefined) {
            //should.equal(actual,expected,message)

            me.validateData("TaskId",recExtractObjectId, variables.extractTaskId,retryCount,logfilename);
            me.validateData("TaskSubject",recExtractObjectId, variables.extractTaskSubject,retryCount,logfilename);
            me.validateData("taskOrganizationName",recExtractObjectId, variables.extractOrgName,retryCount,logfilename);

            //should.equal(recExtractObjectId, variables.extractTaskId, "Task Id didn't match in Calendar");
            //should.equal(recExtractObjectId, variables.extractTaskSubject, "Task Subject didn't match in Calendar");
            //should.equal(recExtractObjectId, variables.extractOrgName, "Task Organization didn't match in Calendar");
        }
    }
    console.log('------------------------------------------------------------------------------')
};
exports.validateCalendarData = validateCalendarData;


//This Function validates Data returned after  Initializing Session
//TODO : Eric, in below logic ill require your help for getting me the .json file for session data of user if possible for comparing our file with returned .json file (if we can have .json file for user this will make this very simple)
var validateSessionData = function (sessionData) {
    console.log('=====> Validating Session Data  : validateSessionData ');

    var jsonData = JSON.stringify(sessionData);
    fs.writeFileSync('sessionData.json',jsonData);//This Creates .json file for storing data returned from session call
    fs.writeFileSync('sessionDataToMatch.json',jsonData);// Currently above file and this refers to same data so if we can have this file already stored in our system this will help

    var data = require('./sessionData.json');
    var newData = require('./sessionDataToMatch.json');

    expect( data).to.deep.equal( newData ); // To compare data stored in both files

    /*
     //Below Code is a sample code which updates 1 file to compare the difference is fond and case is working
     var obj = require('./sessionNewData.json');
     obj.user.name = 'Changed Name';
     fs.writeFile('sessionNewData.json', JSON.stringify(obj), function (err) {
     console.log(err);
     });

     var data = require('./sessionData.json');
     var newData = require('./sessionNewData.json');
     */


    console.log('------------------------------------------------------------------------------')
};
exports.validateSessionData = validateSessionData;

//This function validates new Organization info
var validateOrganizationData = function (dataOfElement,hc1Object,retryCount,logfilename){
    //genericOperations.writeLocalLogs(caseName,should.equal(checkData(dataOfElement.name, hc1Object, "organization.name"),variables.extractOrgName, "organization name value didn't match"),retryCount,logfilename);
    var currentUser = variables.userFirstName+" "+variables.userLastName;
    console.log('=====> Validating Organization Info  : validateOrganizationData');

    me.validateData("organizationName",checkData(dataOfElement.name, hc1Object, "organization.name"),variables.extractOrgName,retryCount,logfilename);
    me.validateData("organizationDescription",checkData(dataOfElement.description, hc1Object, "organization.description"),variables.orgDescription,retryCount,logfilename);
    me.validateData("organizationMailingCity",checkData(dataOfElement.mailing.city, hc1Object, "organization.mailing.city"),variables.mailingAddressCity,retryCount,logfilename);
    me.validateData("organizationMailingState",checkData(dataOfElement.mailing.state, hc1Object, "organization.mailing.state"),variables.mailingAddressState,retryCount,logfilename);
    me.validateData("createdUserName",checkData(dataOfElement.createdUser.name, hc1Object, "organization.createdUser.name"),currentUser,retryCount,logfilename);
    me.validateData("createdUserFirstname",checkData(dataOfElement.createdUser.firstName, hc1Object, "organization.createdUser.firstName"),variables.userFirstName,retryCount,logfilename);
    me.validateData("createdUserLastname",checkData(dataOfElement.createdUser.lastName, hc1Object, "organization.createdUser.lastName"),variables.userLastName,retryCount,logfilename);
    me.validateData("createdUserEmail",checkData(dataOfElement.createdUser.email, hc1Object, "organization.createdUser.email"),variables.userEmail,retryCount,logfilename);
    me.validateData("updateUserName",checkData(dataOfElement.updatedUser.name, hc1Object, "organization.updatedUser.name"),variables.username,retryCount,logfilename);
    me.validateData("updatedUserEmail",checkData(dataOfElement.updatedUser.email, hc1Object, "organization.updatedUser.email"),variables.userEmail,retryCount,logfilename);
    me.validateData("updatedFirstname",checkData(dataOfElement.updatedUser.firstName, hc1Object, "organization.updatedUser.firstName"),variables.userFirstName,retryCount,logfilename);
    me.validateData("updatedUserLastname",checkData(dataOfElement.updatedUser.lastName, hc1Object, "organization.updatedUser.lastName"),variables.userLastName,retryCount,logfilename);

};
exports.validateOrganizationData = validateOrganizationData;

//This function prints the retry logs for the test cases
exports.printRetryLog = function (retryCount, timeout, isPassed, currentCase) {
    if(retryCount <= variables.retryNoOfTimes && isPassed === false){
        console.info('\n\n\n------------------------------------------------------------------------------------------------');
        console.info('=====> RETRYING CASE '+currentCase.currentTest.fullTitle()+' ... '+retryCount+' time with timeout : '+timeout);
        console.info('------------------------------------------------------------------------------------------------\n\n\n');
    }
}


//This function clears the data that is generated by the test cases
exports.clearAllData = function () {
    var len = variables.entitiesToClear.length;
    for (var i = 0; i < variables.entitiesToClear.length; i++) {
        me.clearRecord(variables.entitiesToClear[i]);
    }
}

//This function is the implementation of the above functionality
var clearRecord = function (entityType) {
    describe("Clear record function", function () {
        var timeout = variables.initialTimeout, retryCount = 0, isPassed = false, caseName = logfilename+"-"+this.title;
        afterEach(function () {
            retryCount += 1;
            timeout += variables.increaseTimeout;
            me.printRetryLog(retryCount,timeout,isPassed,this);
        });
        it("should clear " + entityType + " record from system", function () {
                this.retries(variables.retryNoOfTimes);
                this.timeout(timeout);
                var entityId = '', executeClear = false, isdifferentRequest = false, newRequestBody = '', url = '/api/entity/activate';
                switch (entityType) {
                    case 'Organization': {
                        if(variables.extractOrgId != ''){
                            entityId = variables.extractOrgId;
                            entityType = "Organization";
                            executeClear = true;
                        }else{
                            console.log('=====> Organization Id not found ! Organization was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Organization Id not found ! Organization was not retrieved !",retryCount,logfilename);
                            throw new Error("Organization Id not found ! Organization was not retrieved !");
                        }
                        break;
                    }
                    case 'Case': {
                        if(variables.extractCaseId != ''){
                            entityId = variables.extractCaseId;
                            entityType = "Activity";
                            executeClear = true;
                        }
                        else{
                            console.log('=====> Case Id not found ! Case was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Case Id not found ! Case was not retrieved !",retryCount,logfilename);
                            throw new Error("Case Id not found ! Case was not retrieved !");
                        }
                        break;
                    }
                    case 'Task': {
                        if(variables.extractCaseId != ''){
                            entityId = variables.extractTaskId;
                            entityType = "Activity";
                            executeClear = true;
                        }else{
                            console.log('=====> Task Id not found ! Task was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Task Id not found ! Task was not retrieved !",retryCount,logfilename);
                            throw new Error("Task Id not found ! Task was not retrieved !");
                        }
                        break;
                    }
                    case 'Memo': {
                        if(variables.extractMemoId != ''){
                            entityId = variables.extractMemoId;
                            entityType = "Activity";
                            executeClear = true;
                        }else{
                            console.log('=====> Memo Id not found ! Memo was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Memo Id not found ! Memo was not retrieved !",retryCount,logfilename);
                            throw new Error("Memo Id not found ! Memo was not retrieved !");
                        }
                        break;
                    }
                    case 'Campaign': {
                        if(variables.extractCampaignId != ''){
                            entityId = variables.extractCampaignId;
                            entityType = "Campaign";
                            executeClear = true;
                        }else{
                            console.log('=====> Campaign Id not found ! Campaign was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Campaign Id not found ! Campaign was not retrieved !",retryCount,logfilename);
                            throw new Error("Campaign Id not found ! Campaign was not retrieved !");
                        }
                        break;
                    }
                    case 'Contact': {
                        if(variables.extractContactId != ''){
                            entityId = variables.extractContactId;
                            entityType = "Contact";
                            executeClear = true;
                        }else{
                            console.log('=====> Contact Id not found ! Contact was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Contact Id not found ! Contact was not retrieved !",retryCount,logfilename);
                            throw new Error("Contact Id not found ! Contact was not retrieved !");
                        }
                        break;
                    }
                    case 'Provider': {
                        if(variables.extractProviderId != ''){
                            entityId = variables.extractContactId;
                            entityType = "Contact";
                            executeClear = true;
                        }else {
                            console.log('=====> Provider Id not found ! Provider was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons, caseName, retryCount, logfilename, entityType);
                            me.writeLocalLogs(caseName, "Provider Id not found ! Provider was not retrieved !", retryCount, logfilename);
                            throw new Error("Provider Id not found ! Provider was not retrieved !");
                        }
                        break;
                    }
                    case 'Opportunity': {
                        if(variables.extractOpportunityId != ''){
                            entityId = variables.extractOpportunityId;
                            entityType = "Opportunity";
                            executeClear = true;
                        }else{
                            console.log('=====> Opportunity Id not found ! Opportunity was not retrieved !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Opportunity Id not found ! Opportunity was not retrieved !",retryCount,logfilename);
                            throw new Error("Opportunity Id not found ! Opportunity was not retrieved !");
                        }
                        break;
                    }
                    case 'Message': {
                        if(variables.extractMessageId != ''){
                            entityId = variables.extractMessageId;
                            entityType = "Message";
                            executeClear = true;
                        }else{
                            console.log('=====> Message Id not found ! Message was not received !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Message Id not found ! Message was not received !",retryCount,logfilename);
                            throw new Error("Message Id not found ! Message was not received !");
                        }
                        break;
                    }
                    case 'AccessControlTree' : {
                        if(variables.extractTreeID != ''){
                            entityId = variables.extractTreeID;
                            entityType = "AccessControlTree";
                            executeClear = true;
                        }else{
                            console.log('=====> AccessControlTree Id not found ! AccessControlTree was not received !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"AccessControlTree Id not found ! AccessControlTree was not received !",retryCount,logfilename);
                            throw new Error("AccessControlTree Id not found ! AccessControlTree was not received !");
                        }
                        break;
                    }
                    case 'Workflowtemplate' : {
                        if(variables.extractTemplateId != ''){
                            newRequestBody = JSON.stringify({
                                "groupId" : variables.extractTemplateId,
                                "messageType" : "workflowTemplateDeleteRequest"
                            });
                            url = '/api/admin/rulesengine/templates/delete';
                            isdifferentRequest = true;
                            executeClear = true;
                        }else{
                            console.log('=====> Workflowtemplate Id not found ! Workflowtemplate was not received !');
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,"Workflowtemplate Id not found ! Workflowtemplate was not received !",retryCount,logfilename);
                            throw new Error("Workflowtemplate Id not found ! Workflowtemplate was not received !");
                        }
                        break;
                    }
                }
                var requestBody = '';
                if(executeClear){   // if created entity is found with extracted id, then delete it
                    if(isdifferentRequest){ //different delete request for workflowtemplate
                        requestBody = newRequestBody;
                    }else{
                        requestBody = JSON.stringify({
                            "entities": [{
                                "id": entityId,
                                "name": null,
                                "entityType": entityType
                            }],
                            "active": false,
                            "messageType": "entityActivateRequest"
                        });
                    }
                    return me.callRequest(url, requestBody, "clear "+entityType+" record").then(function (response) {
                        if(response.errorCode != 0){
                            variables.failedCases += 1;
                            me.writeLogs(variables.failedCasesCons,caseName,retryCount,logfilename,entityType);
                            me.writeLocalLogs(caseName,response,retryCount,logfilename);
                        }else{
                            isPassed = true;
                            variables.passedCases += 1;
                            console.log('=====> Successfully removed '+entityType+' from the system !');
                            me.writeLogs(variables.passedCasesCons,caseName,retryCount,logfilename,entityType);
                        }
                        expect(response.errorCode).to.equal(0);
                    });
                    resolve();
                }
            }
        );
    });
}
exports.clearRecord = clearRecord;

var initializeGlobalLogger = function () {
   /* describe("", function () {
        it("should check and initialize log file", function () {*/
            console.log("Log File : " + fs.existsSync(variables.logFilePath));
            if (!fs.existsSync(variables.logFilePath)) {
                console.log('@@@@@@@@@@@@@@@@@@@@......................')
                fs.mkdir(variables.logFilePath);
            }
            logging.clearAppenders();
            logging.loadAppender('file');
            logging.addAppender(logging.appenders.file(variables.logFilePath + variables.executionResultFileName), 'results');

    /*  logging.appenders.pattern('-yyyy-MM-dd');
      logging.appenders.alwaysIncludePattern(true);*/

            var logger = logging.getLogger("results");
            variables.logger = logger;
    /*    });
    });*/
}
exports.initializeGlobalLogger = initializeGlobalLogger;

var initializeSpecificLogger = function (testcase) {
    /*if(fs.existsSync(variables.logFilePath+testcase+'.txt')){
        fs.writeFile(variables.logFilePath+testcase+'.txt', '', function (err,data) {
            if (err) {
                return console.log(err);
            }
        });
    }*/

    // testLog.addAppender(testLog.appenders.file(variables.logFilePath+testcase+'.txt'), testcase);
    testLog.addAppender(logging.appenders.file(variables.logFilePath+testcase+'.txt'), testcase, logging.appenders.pattern('-yyyy-MM-dd'),logging.appenders.alwaysIncludePattern(true));
    var locallogger = testLog.getLogger(testcase);

    return locallogger;
}
exports.initializeSpecificLogger = initializeSpecificLogger;


var writeLogs = function(result,functionName,retryCount,logFileName,extraParameter){
    if(result === variables.failedCasesCons){    //failed cases
        if(retryCount > 0){
            if(extraParameter != undefined){
                variables.logger.error('RETRIED CASE FAILED '+retryCount+' time : '+functionName+' -> '+extraParameter+'='+logFileName+'.txt');
            }else{
                variables.logger.error('RETRIED CASE FAILED '+retryCount+' time : '+functionName+' ='+logFileName+'.txt');
            }
        }else{
            if(extraParameter != undefined){
                variables.logger.error('CASE FAILED : '+functionName+' -> '+extraParameter+'='+logFileName+'.txt');
            }else{
                variables.logger.error('CASE FAILED : '+functionName+' = '+logFileName+'.txt');
            }
        }
    }else{  //passed cases
        if(extraParameter != undefined){
            variables.logger.info('CASE PASSED : '+functionName+' -> '+extraParameter);
        }else{
            variables.logger.info('CASE PASSED : '+functionName);
        }
    }

}
exports.writeLogs = writeLogs;

var writeLocalLogs = function (functionName,toWrite,retryCount,logFileName) {
    var localLogger = '';
    if(retryCount === variables.retryNoOfTimes){
        if(localLogger == '' || localLogger == undefined){
            localLogger = me.initializeSpecificLogger(logFileName);
        }
        localLogger.error(functionName+' : \t'+JSON.stringify(toWrite));
    }
}
exports.writeLocalLogs = writeLocalLogs;

var validateData = function(objecttype,value1,value2,retryCount,logfileName){
    var localLogger = '';
    if(retryCount === variables.retryNoOfTimes){
        if(localLogger == '' || localLogger == undefined){
            localLogger = me.initializeSpecificLogger(logfileName);
        }
        if(value1 != value2){
            localLogger.error("Validation error : "+objecttype+" value didn't match ! Actual value : "+value1+", expected value : "+value2);
        }
    }
    should.equal(value1,value2,objecttype+" value didn't match");
}
exports.validateData = validateData;

var writeRatioLogs = function () {
    describe("",function () {
        it("should write ratio logs to a file",function () {
            variables.logger.info(variables.passedCases+" passing !");
            variables.logger.info(variables.failedCases+" failing !");
        });
    });
}
exports.writeRatioLogs = writeRatioLogs;